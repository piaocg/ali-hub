package com.piaocg.alihub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.piaocg.alihub.config.BaseConfig;


@RestController
@RequestMapping("/oss")
public class OssController {
	@Autowired
	private BaseConfig config;
	@RequestMapping("")
	public BaseConfig test() {
		System.out.println(config);
		return config;
	}
}
