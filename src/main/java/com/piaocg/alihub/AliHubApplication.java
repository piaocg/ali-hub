package com.piaocg.alihub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliHubApplication {
	public static void main(String[] args) {
		SpringApplication.run(AliHubApplication.class, args);
	}
}
