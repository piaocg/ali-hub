package com.piaocg.alihub.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


import lombok.Data;

@Data
@Component
@EnableConfigurationProperties
@PropertySource("application.yml")
@ConfigurationProperties(prefix="oss")
public class OssConfig {
	private String bucket;
	private String endPoint;
}
